const ahv13 = require("./modules/ahv13");
const ahv = new ahv13();

console.log("Pascal Cantaluppi - 756.6898.2898.40");
var isValid = ahv.isValid("756.6898.2898.40");
result(isValid);

console.log("Fred Flintstone - 756.7898.2898.40");
isValid = ahv.isValid("756.7898.2898.40");
result(isValid);

function result(isValid) {
  if (isValid) {
    console.log("ok");
  } else {
    console.log("nok");
  }
}
